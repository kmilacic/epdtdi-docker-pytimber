import sys, pydim, time
from collections import OrderedDict
from dimbrowser import DimBrowser

def ashex(string):
    return ':'.join(hex(ord(x))[2:] for x in string)

def print_services(dbr, dip_string):
    gifpp_services = dbr.getServices(dip_string)
    for i in range(gifpp_services):
        service_tuple = next(dbr.getNextService())
        print(("Service {0}: Type of service {1}, service name = {2}, format = {3}".format(i+1,service_tuple[0],service_tuple[1],service_tuple[2])))


class SubInteger:
    def __init__(self, service, dip_service):
        self.service = service
        self.dip_service = dip_service
        self.subscriber = None
        self.current_val = None

    def client_callback(self, val1):
        """
        Callback function for integere services.
        """

        self.current_val = val1

class SubFloat:
    def __init__(self, service, dip_service):
        self.service = service
        self.dip_service = dip_service
        self.subscriber = None
        self.current_val = None

    def client_callback(self, val1):
        """
        Callback function for float services.
        """
        self.current_val = val1

class SubBoolean:
    def __init__(self, service, dip_service):
        self.service = service
        self.dip_service = dip_service
        self.subscriber = None
        self.current_val = None

    def client_callback(self, val1):
        """
        Callback function for boolean services.
        """

        self.current_val = True if ashex(val1) == '1' else False

class SubString:
    def __init__(self, service, dip_service):
        self.service = service
        self.dip_service = dip_service
        self.subscriber = None
        self.current_val = None

    def client_callback(self, val1):
        """
        Callback function for string services.
        """

        self.current_val = val1

def print_vals(subs):
    message = "Current Values:\n"
    for sub in subs:
        message += f"{(subs[sub].service)} current value: {(subs[sub].current_val)}\n"
    message += "\n\n"
    print(message)

def format_service_name(trimmed_arr):
    service_name = trimmed_arr[0].split('/')
    return "_".join(service_name[2:len(service_name)-1])

def format_service_dict(trimmed_arr):
    dip_format = trimmed_arr[2]
    dip_sig_fig = ""
    dip_float_display = ""
    dip_lower_limit = ""
    dip_upper_limit = ""

    # if a float format then format elements correctly
    if dip_format.startswith('float'):
        try:
            dip_sig_fig = int(trimmed_arr[3])
            tmp_dip_format = dip_format.split('-')
            dip_format = tmp_dip_format[0]
            if len(tmp_dip_format) > 1:
                dip_float_display = tmp_dip_format[1]
        except ValueError:
            print("WARNING: Line '" + trimmed_arr[0] + "' of the CSV file has an invalid value for DIP Significant Figure. Setting to default value instead.")
            dip_sig_fig = 6
        except TypeError:
            print("WARNING: Line '" + trimmed_arr[0] + "' of the CSV file has an incorrect format or missing value for DIP Significant Figure. Setting to empty string instead.")

    if dip_format.startswith("float") or dip_format == "int":
        if trimmed_arr[4] != "":
            try:
                dip_lower_limit = float(trimmed_arr[4])
            except:
                print("WARNING: Line '" + trimmed_arr[0] + "' of the CSV file has an incorrect format or missing value for DIP Lower Limit. Setting to empty string instead.")
        if trimmed_arr[5] != "":
            try:
                dip_upper_limit = float(trimmed_arr[5])
            except:
                print("WARNING: Line '" + trimmed_arr[0] + "' of the CSV file has an incorrect format or missing value for DIP Upper Limit. Setting to empty string instead.")
    elif dip_format == "boolean":
        if trimmed_arr[4] == "True" and trimmed_arr[5] == "False":
            dip_lower_limit = True
            dip_upper_limit = False
        else:
            dip_lower_limit = False
            dip_upper_limit = True

    return {'dip_path': trimmed_arr[0], 'dip_unit': trimmed_arr[1], 'dip_format': dip_format, 'dip_sig_fig': dip_sig_fig, 'dip_float_display': dip_float_display, 'dip_lower_limit': dip_lower_limit, 'dip_upper_limit': dip_upper_limit, 'dip_group': trimmed_arr[6]}

def form_service_dict(keyFunction, valFunction, values):
    return dict((keyFunction(v), valFunction(v)) for v in values)

def dip_listen(dip_configured):
    formatter = {
        "int": ["I:1", SubInteger],
        "float": ["F:1", SubFloat],
        "boolean": ["C:1", SubBoolean],
        "string": ["C", SubString]
    }
    dip_services = {}
    dip_groups = {}

    for app_key, dip_app in dip_configured.items():
        group = ""
        groups = []
        formatted_app = []
        for line_no, line in enumerate(dip_app):
            if line.startswith("# "):
                group = line.split("# ")[1].strip()
                groups.append(group)
            else:
                trimmed_arr = line.strip().split(',')
                # ensure any service arrays have all 6 elements to avoid errors
                trimmed_arr = trimmed_arr[:6] + [" "]*(6 - len(trimmed_arr))
                trimmed_arr.append(group)
                formatted_app.append(trimmed_arr)
        dip_service_dict = form_service_dict(format_service_name, format_service_dict, formatted_app)
        dip_services[app_key] = OrderedDict(sorted(dip_service_dict.items(), key=lambda t: t[0]))
        dip_groups[app_key] = groups
        for service_key, service in dip_services[app_key].items():
            if service['dip_format'] in formatter:
                format = formatter[service['dip_format']]
                service['dip_sub'] = format[1](service_key, service['dip_path']);
                service['dip_sub'].subscriber = pydim.dic_info_service(service['dip_path'], format[0], service['dip_sub'].client_callback, default_value=None)

    return dip_services, dip_groups
