var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477',
'#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262',
'#5574A6','#3B3EAC'];

var ctx = document.getElementById("trPlot").getContext('2d');
var plot = document.getElementById('chartContainer');

var chartJSRendered = false;
var canvasJSRendered = false;
var totalPoints = 0;

var data = JSON.parse(plot.dataset.points);
var vars = JSON.parse(plot.dataset.vars.replace(/'/g, '"'));
var units = JSON.parse(plot.dataset.units.replace(/'/g, '"'));
var types = JSON.parse(plot.dataset.types.replace(/'/g, '"'));

var vars_units = vars.map(function(variable, i) {
  return (units[i] !== " ") ? [variable + " [" + units[i] + "]"] : [variable]
});

// CanvasJS dataset initialising
let chartData = vars.map(function(variable, i) {
  let points = data[variable][0].map(function(e, j) {
    let datetime = new Date(e * 1000);
    return {
      x: datetime,
      y: data[variable][1][j],
      label: datetime.toLocaleString()
    }
  });
  totalPoints += points.length;
  return {
    showInLegend: true,
    legendText: (units[i] !== " ") ? variable + " [" + units[i] + "]" : variable,
    // check if variable is in dontInterpol array, if so don't interpolate
    type: (types[i] === "boolean") ? 'stepLine' : 'line',
    dataPoints: points
  }
});

var canvasJS = new CanvasJS.Chart("chartContainer",
{
  zoomEnabled: true,
  exportEnabled: true,
  animationEnabled: true,
  animationDuration: 2500,
  title: {
    fontSize: 20,
    text: "Plot of " + vars.join(", ") + " over Datetime"
  },
  legend: {
    fontSize: 14,
    cursor: "pointer",
    itemclick: function (e) {
      if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
      } else {
          e.dataSeries.visible = true;
      }
      e.chart.render();
    }
  },
  axisX: {
    title: "Datetime",
    titleFontSize: 16,
    labelFontSize: 12
  },
  axisY: {
    titleFontSize: 16,
    labelFontSize: 12,
    title: vars_units.join(", "),
    includeZero: false
  },
  data: chartData,

});

function renderChartJS() {
  if (chartJSRendered) {
    chartJSRendered = false;
    document.getElementById("trPlot").style.display = "none";
  } else {
    // ChartJS dataset initialising
    let datasets = vars.map(function(variable, i) {
      let points = data[variable][0].map(function(e, j) {
        return {
          x: e * 1000,
          y: data[variable][1][j]
        }
      });
      return {
        label: (units[i] !== " ") ? variable + " [" + units[i] + "]" : variable,
        data: points,
        fill: false,
        borderColor: default_colors[i % default_colors.length],
        steppedLine: (types[i] === "boolean") ? true : false
      }
    });

    chartJSRendered = true;
    document.getElementById("trPlot").style.display = "block";
    var trPlot = new Chart(ctx, {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        title: {
           display: true,
           text: "Plot of " + vars.join(", ") + " over Datetime"
        },
        scales: {
           xAxes: [{
             type: 'time',
             time: {
               unit: 'day',
               displayFormats: {
                  'day': 'DD MMM YYYY'
               }
             }
           }],
           yAxes: [{
             scaleLabel: {
               display: true,
               labelString: vars_units.join(", ")
             }
           }]
        },
        elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        },
     }
    });
  }
}

function renderCanvasJS() {
  if (canvasJSRendered) {
    canvasJSRendered = false;
    document.getElementById("chartContainer").style.display = "none";
  } else {
    canvasJSRendered = true;
    document.getElementById("chartContainer").style.display = "block";
    canvasJS.render();
  }
}
