from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:app_vars>', views.plot_index, name='plot_index'),
    path('<str:app_vars>/dip', views.dip, name='dip'),
    path('<str:app_vars>/dip/vals', views.dipVals, name='dipVals'),
    path('<str:app_vars>/hello/<str:name>', views.hello, name="hello"),
    path('<str:app_vars>/trPlot', views.trPlot, name="trPlot"),
    path('<str:app_vars>/trFile', views.trStreamingFile, name="trStreamingFile")
]
