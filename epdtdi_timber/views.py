import csv, json, numpy

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, StreamingHttpResponse
from epdtdi_timber.utils import userAuthorised, getToday, getUTC, getGroupedVars, makeTrPlot, getData, getDipVals
from django.conf import settings

class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value

def index(request):
    apps_available = settings.APPS_CONFIGURED.keys()
    app_error = request.GET.get('app_error', '')
    group_error = request.GET.get('group_error', '')
    context = {
        "apps_available": sorted(apps_available),
        "app_error": app_error,
        "group_error": group_error,
    }
    return render(request, 'epdtdi_timber/index.html', context)

def plot_index(request, app_vars=settings.DEFAULT_APP_NAME):
    """An index view that first offers the user the ability to request data."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # set timestamps for today
    fromTS, toTS = getToday()
    # request the app's variables grouped into categories
    groupedVars = getGroupedVars(app_vars)
    # define selected variables
    selectedVariables=[]
    # set the context dictionary for Django's view
    context = {
        "varList": groupedVars,
        "selectedVars": selectedVariables,
        "days": 1,
        "fromUTC": 0,
        "toUTC": 0,
        "fromDTPValue": fromTS,
        "toDTPValue": toTS,
        "what": None,
        "plot_path": "timber-logs.jpg",
        'app_vars': app_vars
    }
    # render the request in a view, using context
    return render(request, 'epdtdi_timber/plots.html', context)

def hello(request, app_vars=settings.DEFAULT_APP_NAME, name=""):
    """A test-view that simply renders data from the request URL."""
    # render the hello test view with the user's name
    return render(request, 'epdtdi_timber/hello.html', {
        'name': name,
        'app_vars': app_vars
    })

def trPlot(request, app_vars=settings.DEFAULT_APP_NAME):
    """A view that returns two plots to the user, mapping the requested data
       visually onto 1 matplotlib PNG plot, 1 ChartJS interactive HTML5 canvas
       plot and 1 CanvasJS interactive HTML5 div plot. The ChartJS & CanvasJS
       plots have to be drawn client-side so the data is simply
       provided to the browser via the relevant Django template."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # request the app's variables grouped into categories
    groupedVars = getGroupedVars(app_vars)
    # collect the requested vars from the POST request as a list ~may be empty~
    vars = request.POST.getlist('vars')
    static_vars= request.POST.getlist('vars')
    # collect from datetime from POST request, if empty will be False
    fromDTP = request.POST.get('fromDTP', False)
    # collect to datetime from POST request, if empty will be False
    toDTP = request.POST.get('toDTP', False)
    # should either datetime not be defined redirect to index
    if not fromDTP or not toDTP:
        return redirect('index')
    # convert datetimes to UTC format
    fromUTCsecs, toUTCsecs = getUTC(fromDTP, toDTP)
    # call util function to generate the matplotlib png and return its path
    # along with the data collected and the request elapsed time
    plot_path, data, elapsed_time, vars_units, vars_types = makeTrPlot(fromUTCsecs, toUTCsecs, vars, app_vars)
    # loop through returned data and convert numpy ndarrays to python standard
    # arrays - since they meet the JSON standard
    for var in vars:
        data[var] = (data[var][0].tolist(), data[var][1].tolist())
    # collect units for each variable in use into a single array
    units = [vars_units[x] for x in vars]
    # collect types for each variable in use into a single array
    types = [vars_types[y] for y in vars]
    # convert sanitized data to JSON format for JS to interpret client-side
    data = json.dumps(data)
    # set the context dictionary for Django's view
    context = {
        "varList": groupedVars,
        "selectedVars": static_vars,
        "dataVars": vars,
        "days": 1,
        "fromUTC": 0,
        "toUTC": 0,
        "fromDTPValue": fromDTP,
        "toDTPValue": toDTP,
        "what": None,
        "plot_path": plot_path,
        "data": data,
        "units": units,
        "types": types,
        'app_vars': app_vars
    }
    # render the request in a view, using context
    response = render(request, 'epdtdi_timber/plots.html', context)
    # set cookie on client confirming elapsed DB request time
    response.set_cookie("dataRequestTime", value=elapsed_time)
    # return the crafter response
    return response

def trStreamingFile(request, app_vars=settings.DEFAULT_APP_NAME):
    """A 'view' that streams a large CSV file of the requested data."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # collect the requested vars from the POST request as a list ~may be empty~
    vars = request.POST.getlist('vars')
    # collect from datetime from POST request, if empty will be False
    fromDTP = request.POST.get('fromDTP', False)
    # collect to datetime from POST request, if empty will be False
    toDTP = request.POST.get('toDTP', False)
    # should either datetime not be defined redirect to index
    if not fromDTP or not toDTP:
        return redirect('index')
    # convert datetimes to UTC format
    fromUTCsecs, toUTCsecs = getUTC(fromDTP, toDTP)
    # if no vars provided no CSV can be generated, so send fail response
    if vars is None or len(vars) < 1:
        response = HttpResponseBadRequest("Failed to provide variables.")
        response.set_cookie("fileFailed", value="False")
        return response
    # call util function to request required data from the DB and collect
    # the DB request elapsed time
    data, elapsed_time, vars_units = getData(fromUTCsecs, toUTCsecs, vars, app_vars)
    # if no data returned, fail gracefully for user and inform browser
    if data is None:
        response = HttpResponseBadRequest("No data available.")
        response.set_cookie("fileFailed", value="False")
        return response
    # start formatting of data to CSV by initialising rows var
    rows = [[""]]
    # loop through vars, enumerating the index for each as well
    for var_idx, var in enumerate(vars):
        # add new header cells for the two new columns for current var
        rows[0].extend([var + "_tt", var + "_vv_" + vars_units[var]])
        # retrieve each column from the data for current var
        tt, vv = data[var]
        # since tt & vv will be same length (as defined by pytimber), loop through
        # index of tt and use index to access both
        for idx in range(len(tt)):
            # if rows length is greater than index (shifted by 1 to accommodate first column
            # of row numbers) then continue adding entries
            if len(rows) > idx + 1:
                # collect current row
                row = rows[idx + 1]
                # if row length is the right length (has 1 row column & 2 columns for every previous variable)
                if len(row) == (var_idx * 2) + 1:
                    # add entries for the two new columns
                    row.extend([tt[idx], vv[idx]])
                # else row length is too short and must add empty values to the left first, to accommodate
                # the columns that have less entries
                else:
                    # add correct number of empty values (2 per variable that is missing)
                    # must be aware that some columns to the left may exist, so account for such event by
                    # normalising with the length of the row (-1 for the first column)
                    row.extend([""] * ((var_idx * 2) - (len(row) - 1)))
                    # add entries for the two new columns
                    row.extend([tt[idx], vv[idx]])
            # not enough rows presently to add entries, must append new row
            # then add necessary empty values to align properly
            else:
                # create new row with "Row X" first cell
                rows.append(["Row {}".format(idx + 1)])
                # collect new row for extending
                row = rows[idx + 1]
                # add correct number of empty values (2 per variable that is missing)
                row.extend([""] * (var_idx * 2))
                # add entries for the two new columns
                row.extend([tt[idx], vv[idx]])
    # initialise CSV stream writer with a pseudo buffer that better supports streams
    pseudo_buffer = Echo()
    # setup csv writer around the pseudo_buffer
    writer = csv.writer(pseudo_buffer)
    # start the stream response to client that should better support large files
    response = StreamingHttpResponse((writer.writerow(row) for row in rows),
                                     content_type="text/csv")
    # identify response as file via headers to the browser/client
    response['Content-Disposition'] = 'attachment; filename="pytimber.csv"'
    # set cookie with DB request elapsed time
    response.set_cookie("fileDownloaded", value=elapsed_time)
    # return the crafted response to client
    return response

def dip(request, app_vars=settings.DEFAULT_APP_NAME):
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    if app_vars in settings.DIP_CONFIGURED:
        dipData = getDipVals(app_vars)
    else:
        dipData = []
    if app_vars in settings.SUB_GROUPS:
        dipGroups = settings.SUB_GROUPS[app_vars]
    else:
        dipGroups = []
    context = {
        "plot_path": "timber-logs.jpg",
        'app_vars': app_vars,
        'dipData': dipData,
        'dipGroups': dipGroups
    }
    return render(request, 'epdtdi_timber/plots.html', context)

def dipVals(request, app_vars=settings.DEFAULT_APP_NAME):
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    if app_vars in settings.DIP_CONFIGURED:
        data = getDipVals(app_vars)
    else:
        data = []
    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type="application/json")
